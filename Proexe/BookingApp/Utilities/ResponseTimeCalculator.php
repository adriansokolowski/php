<?php

/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface
{

	/**
	 * @param string $bookingDateTime
	 * @param string $responseDateTime
	 * @param array $officeHours
	 * 
	 * @return int
	 */
	public function calculate($bookingDateTime, $responseDateTime, $officeHours): int
	{
		$datetime1 = date_create($bookingDateTime);
		$datetime2 = date_create($responseDateTime);

		$hours = [];
		foreach ($officeHours as $key => $value) {
			if ($value['isClosed'] === false) {
				$hours[$key][] = abs(strtotime($value['from']) % 86400);
				$hours[$key][] = abs(strtotime($value['to']) % 86400);
			} else {
				$hours[$key] = null;
			}
		}

		return ($this->getDifference($datetime1, $datetime2, $hours));
	}

	/**
	 * @param object $start
	 * @param object $end
	 *
	 * @return int
	 */
	private function getDifference(object $start, object $end, array $working_hours): int
	{
		$seconds = 0;

		$start_date = clone $start;
		$start_date = $start_date->setTime(0, 0, 0)->getTimestamp();
		$start_time = $start->getTimestamp() - $start_date;

		$end_date = clone $end;
		$end_date = $end_date->setTime(0, 0, 0)->getTimestamp();
		$end_time = $end->getTimestamp() - $end_date;

		for ($today = $start_date; $today <= $end_date; $today += 86400) {

			$today_weekday = date('w', $today);

			if (!isset($working_hours[$today_weekday][0]) || !isset($working_hours[$today_weekday][1])) continue;

			$today_start = $working_hours[$today_weekday][0];
			$today_end = $working_hours[$today_weekday][1];

			if ($today === $start_date) $today_start = min($today_end, max($today_start, $start_time));
			if ($today === $end_date) $today_end = max($today_start, min($today_end, $end_time));

			$seconds += $today_end - $today_start;
		}

		return floor($seconds / 60);
	}
}
