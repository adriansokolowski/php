<?php

/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator
{

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return float
	 */
	public function calculate($from, $to, $unit = 'm'): float
	{
		$distance = $this->distance($from, $to);
		return ($unit == 'm') ? $distance['m'] : $distance['km'];
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return string
	 */
	public function findClosestOffice($from, $offices): string
	{
		$dist = array_map(fn ($item) => $this->distance($from, [$item['lng'], $item['lat']]), $offices);

		asort($dist);

		return $offices[key($dist)]['name'];
	}

	/**
	 * @param array $x
	 * @param array $y
	 *
	 * @return array
	 */
	private function distance(array $x, array $y): array
	{
		list($lat1, $lon1) = $x;
		list($lat2, $lon2) = $y;

		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$km = $miles * 1.609344;
		$m = $km * 1000;

		return compact('km', 'm');
	}
}

